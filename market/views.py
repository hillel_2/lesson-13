from django.http import HttpResponse
from django.shortcuts import render, redirect

from market.forms import WallpaperForm
from market.models import Wallpaper


def list(request):
    wallpapers = Wallpaper.objects.all()
    return render(request,
                  'market/wallpapers.html',
                  context={'wallpapers': wallpapers})


def item(request, item_id):
    wallpaper = Wallpaper.objects.get(pk=item_id)
    return render(request,
                  'market/wallpaper.html',
                  context={'wallpaper': wallpaper})


def add(request):
    if request.method == 'POST':
        form = WallpaperForm(request.POST, request.FILES)
        if form.is_valid():
            new_wallpaper = form.save()
            return redirect('market:item', item_id=new_wallpaper.pk)
        return render(request, 'market/new_wallpaper.html', context={'form': form})
    form = WallpaperForm()
    return render(request, 'market/new_wallpaper.html', context={'form': form})
